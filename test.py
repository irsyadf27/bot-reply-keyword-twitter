#!/usr/bin/python
# Author: poticous

import time
import sqlite3
import twitter
from TwitterSearch import *

# Setting
consumer = ['***', '***']
token = ['*****', '*****']
keywords = ['persib']
lang = 'id'
text_reply = 'test'

api = twitter.Api(consumer_key=consumer[0],
                      consumer_secret=consumer[1],
                      access_token_key=token[0],
                      access_token_secret=token[1])

conn = sqlite3.connect("database.db")
sql = conn.cursor()
try:
	tso = TwitterSearchOrder()
	tso.set_keywords(keywords)
	tso.set_language(lang)
	tso.set_include_entities(False)

	ts = TwitterSearch(
		consumer_key = consumer[0],
		consumer_secret = consumer[1],
		access_token = token[0],
		access_token_secret = token[1]
	)

	for tweet in ts.search_tweets_iterable(tso):
		try:
			print( '@%s tweeted: %s' % ( tweet['user']['screen_name'], tweet['text'] ) )
			pola = str('@' + str(tweet['user']['screen_name']) + ' ' + str(text_reply))
			reply = api.PostUpdate(pola, in_reply_to_status_id=tweet['id_str'])
			data = (tweet['id_str'], tweet['user']['id_str'], tweet['user']['screen_name'], tweet['text'], tweet['created_at'], time.strftime("%c"), pola)
			print sql.execute("INSERT INTO logs VALUES (?, ?, ?, ?, ?, ?, ?)", data)
			print sql.execute("SELECT * FROM logs").fetchall()
		except:
			pass
except TwitterSearchException as e:
	print(e)
